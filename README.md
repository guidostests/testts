# README #

This project implements at a basic level the mandatory requirements:

* AS 2.2.3, build tools and SDK 25, android support
* 3rd party libs: retrofit, gson,  parceler, glide, mockito, junit4
* UI/UX is basic, but can be easily expanded and configured, changing recycler's LayoutManager and item layout
* same of the metadata have been shown for simplicity, the others can be added in a very similar way, although there are two specific cases to handle:
    * date_taken: requires a custom converter in gson
    * description: contains xml so it should be rendered properly
* all commits available in bitbucket

As for the optional requirements:

* Image caching is handled by glide, by default it caches resized images
* tapping on the image will open it in the browser - if available on the device

During the development I have focused specifically on:

* architecture: I'm using the MVP pattern, based on a library that I'm developing: https://github.com/guidovezzoni/MVP
* Unit Testing: I tried to cover the Presenter and some helper classes
* commented part of the code that are not intuitive or self-explanatory
* committing regularly, especially whenever a feature was completed or before a big change - that could possibly require to be scrapped and restart from scratch