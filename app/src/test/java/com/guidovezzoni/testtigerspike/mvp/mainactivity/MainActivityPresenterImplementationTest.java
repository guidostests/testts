package com.guidovezzoni.testtigerspike.mvp.mainactivity;

import com.guidovezzoni.mvp.ExtendedContract;
import com.guidovezzoni.testtigerspike.model.FlickrFeed;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.runners.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;

import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.verify;

/**
 * Created by guido on 16/12/16.
 */
@RunWith(MockitoJUnitRunner.class)
public class MainActivityPresenterImplementationTest {
    private MainActivityPresenterImplementation mPresenter;

    @Mock
    private MainActivity mockMainActivity;

    @Mock
    private MainActivityModelImplementation mockModel;

    @Mock
    FlickrFeed mockFlickrFeed;

    @Mock
    ExtendedContract.Model.OnModelListener<FlickrFeed> mockListener;

    @Before
    public void setUp() throws Exception {
        mPresenter = new MainActivityPresenterImplementation(mockModel);
        mPresenter.attachView(mockMainActivity);

    }

    @After
    public void tearDown() throws Exception {

    }

    /* *****************************************************************************
    The following four tests are all for viewNeedsData, but considering 4 different behaviours, depending
    on the internal state or result from the model.
    Model and Listeners from the model have been mocked
    */

    @Test
    public void viewNeedsData_FromSavedState() throws Exception {
        mPresenter.setSavedInstanceState(mockFlickrFeed);

        mPresenter.viewNeedsData();

        verify(mockMainActivity).dataRequestSuccessful(mockFlickrFeed);
    }

    @Test
    public void viewNeedsData_Success() throws Exception {
        doAnswer(new Answer() {
            public Object answer(InvocationOnMock invocation){
                Object[] args = invocation.getArguments();
                ExtendedContract.Model.OnModelListener<FlickrFeed> listener = (ExtendedContract.Model.OnModelListener<FlickrFeed>) args[1];
                listener.onDataRetrieved(mockFlickrFeed);
                return null;
            }
        }).when(mockModel).retrieveData(Matchers.any(Void.class), Mockito.any(ExtendedContract.Model.OnModelListener.class));

        mPresenter.viewNeedsData();

        verify(mockMainActivity).dataRequestSuccessful(mockFlickrFeed);
    }

    @Test
    public void viewNeedsData_Error() throws Exception {
        doAnswer(new Answer() {
            public Object answer(InvocationOnMock invocation){
                Object[] args = invocation.getArguments();
                ExtendedContract.Model.OnModelListener<FlickrFeed> listener = (ExtendedContract.Model.OnModelListener<FlickrFeed>) args[1];
                listener.onDataUnavailable("Error msg");
                return null;
            }
        }).when(mockModel).retrieveData(Matchers.any(Void.class), Mockito.any(ExtendedContract.Model.OnModelListener.class));

        mPresenter.viewNeedsData();

        verify(mockMainActivity).dataRequestError("Error msg");
    }

    @Test
    public void viewNeedsData_Cancel() throws Exception {
        doAnswer(new Answer() {
            public Object answer(InvocationOnMock invocation){
                Object[] args = invocation.getArguments();
                ExtendedContract.Model.OnModelListener<FlickrFeed> listener = (ExtendedContract.Model.OnModelListener<FlickrFeed>) args[1];
                listener.onRequestCancelled();
                return null;
            }
        }).when(mockModel).retrieveData(Matchers.any(Void.class), Mockito.any(ExtendedContract.Model.OnModelListener.class));

        mPresenter.viewNeedsData();

        verify(mockMainActivity).dataRequestCancelled();
    }

}