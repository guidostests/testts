package com.guidovezzoni.testtigerspike.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.guidovezzoni.testtigerspike.R;
import com.guidovezzoni.testtigerspike.model.ItemFeed;
import com.guidovezzoni.testtigerspike.utils.DateHelper;
import com.guidovezzoni.testtigerspike.viewholder.FlickrFeedViewHolder;

import java.util.List;

public class FlickrFeedAdapter extends RecyclerView.Adapter<FlickrFeedViewHolder> {
    private List<ItemFeed> mList;
    private Context mContext;
    private AdapterListener mListener;

    public FlickrFeedAdapter(Context context, List<ItemFeed> list, @NonNull AdapterListener adapterListener) {
        if (adapterListener == null) {
            throw new NullPointerException("AdapterListener");
        }

        mContext = context;
        mList = list;
        mListener = adapterListener;
    }

    @Override
    public FlickrFeedViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_item_feed, parent, false);
        return new FlickrFeedViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(FlickrFeedViewHolder holder, int position) {
        final ItemFeed itemFeed = mList.get(position);

        holder.mTitle.setText(itemFeed.title != null ? itemFeed.title : "");
        holder.mDatePublished.setText(DateHelper.getFormattedDate(DateHelper.TIME_FORMAT, itemFeed.published));
        holder.mAuthor.setText(itemFeed.author != null ? itemFeed.author : "");

        // IMAGE CACHING: Glides caches images by default
        Glide.with(mContext)
                .load((itemFeed.media != null && itemFeed.media.m != null) ? itemFeed.media.m : "")
                .centerCrop()
                .placeholder(R.drawable.ic_cached_black_24dp)
                .into(holder.mImage);
        holder.mImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onOpenBrowser(itemFeed.link);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public interface AdapterListener {

        void onOpenBrowser(String link);

        void onShareImage();
    }
}
