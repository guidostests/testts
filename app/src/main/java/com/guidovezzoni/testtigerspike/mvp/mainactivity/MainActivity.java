package com.guidovezzoni.testtigerspike.mvp.mainactivity;

import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.widget.Toast;

import com.guidovezzoni.testtigerspike.R;
import com.guidovezzoni.testtigerspike.adapter.FlickrFeedAdapter;
import com.guidovezzoni.testtigerspike.model.FlickrFeed;
import com.guidovezzoni.testtigerspike.utils.ShareHelper;

import org.parceler.Parcels;

/**
 * Created by guido on 15/12/16.
 */

/**
 * MainActivity uses an MVP structure. I've used a very simple MVP library that I've been working on
 * recently and that is available online at https://github.com/guidovezzoni/MVP
 * We have:
 * - MainActivityContract which collects the interfaces for M, V, and P
 * - MainActivity, the standard AS Activity that implements V
 * - MainActivityPresenterImplementation implementing P
 * - MainActivityModelImplementation implementing M
 */
public class MainActivity extends AppCompatActivity implements MainActivityContract.View {
    private final static String KEY_FLICKRFEED = "FLICKRFEED";

    private MainActivityContract.Presenter mPresenter;

    private Toolbar mToolbar;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private RecyclerView mRecyclerView;

    @Override
    public MainActivityContract.Presenter getPresenter() {
        return mPresenter;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mPresenter = new MainActivityPresenterImplementation(new MainActivityModelImplementation());

        // pass data to presenter for change of configurations, etc.
        if (savedInstanceState != null) {
            Parcelable parcelable = savedInstanceState.getParcelable(KEY_FLICKRFEED);
            if (parcelable!=null) {
                mPresenter.setSavedInstanceState((FlickrFeed) Parcels.unwrap(parcelable));
            }
        }

        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);


        mSwipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipeRefreshLayout);
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                mPresenter.viewNeedsData();
            }
        });

        mRecyclerView = (RecyclerView) findViewById(R.id.list_recyclerView);

        // load data
        mPresenter.attachView(this);
        mSwipeRefreshLayout.setRefreshing(true);
        mPresenter.viewNeedsData();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mPresenter.detachView();
    }

    // save data before change of configurations, etc.
    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelable(KEY_FLICKRFEED, Parcels.wrap(mPresenter.getDataForSaveInstanceState()));
    }

    /**
     * this method is called from presenter when data to show is available
     *
     * @param flickrFeed downloaded data
     */
    @Override
    public void dataRequestSuccessful(FlickrFeed flickrFeed) {
        mRecyclerView.setAdapter(new FlickrFeedAdapter(this, flickrFeed.items, new FlickrFeedAdapter.AdapterListener() {
            @Override
            public void onOpenBrowser(String link) {
                mPresenter.viewRequestsOpenLink(link);
            }

            @Override
            public void onShareImage() {

            }
        }));
        mSwipeRefreshLayout.setRefreshing(false);
    }

    /**
     * called from presenter when an error happens
     *
     * @param message
     */
    @Override
    public void dataRequestError(String message) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
        Log.e("MainActivity", message);
        mSwipeRefreshLayout.setRefreshing(false);
    }

    /**
     * network request was cancelled, f.i. due to a config change
     */
    @Override
    public void dataRequestCancelled() {
        mSwipeRefreshLayout.setRefreshing(false);
    }


    public void openLinkInBrowser(String link){
        ShareHelper.openBrowser(this, link);
    }

}
