package com.guidovezzoni.testtigerspike.mvp.mainactivity;

import com.guidovezzoni.mvp.ExtendedContract;
import com.guidovezzoni.testtigerspike.model.FlickrFeed;

/**
 * Created by guido on 10/12/16.
 */

public interface MainActivityContract
        extends ExtendedContract<FlickrFeed, Void,
        MainActivityContract.View, MainActivityContract.Presenter, MainActivityContract.Model> {

    interface View extends ExtendedContract.View<FlickrFeed, Presenter> {
        void openLinkInBrowser(String link);
    }

    interface Presenter extends ExtendedContract.Presenter<View, Model> {

        FlickrFeed getDataForSaveInstanceState();

        void setSavedInstanceState(FlickrFeed flickrFeed);

        void viewRequestsOpenLink(String link);
    }

    interface Model extends ExtendedContract.Model<Void, Presenter> {

    }

}
