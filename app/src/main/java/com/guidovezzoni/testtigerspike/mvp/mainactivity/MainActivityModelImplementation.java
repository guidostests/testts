package com.guidovezzoni.testtigerspike.mvp.mainactivity;

import android.support.annotation.NonNull;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.guidovezzoni.mvp.BaseModelImplementation;
import com.guidovezzoni.testtigerspike.api.Api;
import com.guidovezzoni.testtigerspike.api.FlickrService;
import com.guidovezzoni.testtigerspike.model.FlickrFeed;

import java.io.IOException;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by guido on 15/12/16.
 */

public class MainActivityModelImplementation
        extends BaseModelImplementation<MainActivityContract.Presenter>
        implements MainActivityContract.Model {

    private static final String BASE_URL = "https://api.flickr.com/";
    private static final String SERVICE_ERROR = "Service error";

    private FlickrService mFlickrService;
    private Call<FlickrFeed> mFlickrFeedCall;

    @Override
    public void attachPresenter(MainActivityContract.Presenter basePresenter) {
        super.attachPresenter(basePresenter);

        mFlickrService = getFlickrService();
    }

    /**
     * start network request and handle retrofit callbacks
     *
     * @param arg
     * @param onModelListener
     */
    @Override
    public void retrieveData(Void arg, @NonNull final OnModelListener onModelListener) {
        if (onModelListener == null) {
            throw new NullPointerException("onModelListener");
        }

        mFlickrFeedCall = mFlickrService.getPhotoPublic(Api.QUERY_FORMAT_JSON, Api.QUERY_NOJSONCALLBACK_1);
        mFlickrFeedCall.enqueue(new Callback<FlickrFeed>() {
            @Override
            public void onResponse(Call<FlickrFeed> call, Response<FlickrFeed> response) {
                if (response.isSuccessful()) {
                    onModelListener.onDataRetrieved(response.body());
                } else {

                    // TODO this could be more sophisticated....
                    String errorMsg;
                    try {
                        errorMsg = response.errorBody().string();
                    } catch (IOException e) {
                        errorMsg = SERVICE_ERROR;
                    }
                    onModelListener.onDataUnavailable(errorMsg);
                }
            }

            @Override
            public void onFailure(Call<FlickrFeed> call, Throwable t) {
                if (call.isCanceled()) {
                    // this case will occur when retrofit request gets cancelled
                    onModelListener.onRequestCancelled();
                } else {
                    onModelListener.onDataUnavailable(t.getMessage());
                }
            }
        });
    }

    @Override
    public void cancelAsyncRequests() {
        // in case it happened before the Call has been created
        if (mFlickrFeedCall != null) {
            mFlickrFeedCall.cancel();
        }
    }

    /**
     * build retrofit service with additional code to handle timestamps and logs
     *
     * @return
     */
    private FlickrService getFlickrService() {

        // handle logs
        OkHttpClient okHttpClient = new OkHttpClient
                .Builder()
                .addInterceptor(new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY))
                .build();

        // handle timestamps
        // TODO doesn't work for date_taken though as the format is not recognised by SimpleDateFormat
        // need to create a custom converter
        Gson gson = new GsonBuilder()
                .setDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'")
                .create();

        /*
        GsonBuilder builder = new GsonBuilder();

        builder.registerTypeAdapter(Date.class, new JsonDeserializer<Date>() {

            final DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
            @Override
            public Date deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
                try {
                    return df.parse(json.getAsString());
                } catch (final java.text.ParseException e) {


                    return null;
                }
            }
        });
        Gson gson = builder.create();
        */

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

        return retrofit.create(FlickrService.class);
    }

}
