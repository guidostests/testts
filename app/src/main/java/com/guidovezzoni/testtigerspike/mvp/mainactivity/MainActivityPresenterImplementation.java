package com.guidovezzoni.testtigerspike.mvp.mainactivity;

import com.guidovezzoni.mvp.BasePresenterImplementation;
import com.guidovezzoni.mvp.ExtendedContract;
import com.guidovezzoni.testtigerspike.model.FlickrFeed;

/**
 * Created by guido on 15/12/16.
 */

public class MainActivityPresenterImplementation
        extends BasePresenterImplementation<MainActivityContract.View, MainActivityContract.Model>
        implements MainActivityContract.Presenter {

    private FlickrFeed mCurrentData;
    private FlickrFeed mDataFromSavedState;

    public MainActivityPresenterImplementation(MainActivityContract.Model model) {
        super(model);
    }

    /**
     * simply refresh data
     */
    @Override
    public void viewNeedsData() {

        // if there is data from SavedState, restore that rather than re-executing the query
        if (mDataFromSavedState != null) {
            if (getView() != null) {
                mCurrentData = mDataFromSavedState;

                // set this to null so next time the network call will actaully be re-executed
                getView().dataRequestSuccessful(mCurrentData);
            }
            mDataFromSavedState = null;
            return;
        }

        getModel().retrieveData(null, new ExtendedContract.Model.OnModelListener<FlickrFeed>() {
            @Override
            public void onDataRetrieved(FlickrFeed data) {
                if (getView() != null) {
                    mCurrentData = data;
                    getView().dataRequestSuccessful(data);
                }
            }

            @Override
            public void onDataUnavailable(String message) {
                if (getView() != null) {
                    getView().dataRequestError(message);
                }
            }

            @Override
            public void onRequestCancelled() {
                if (getView() != null) {
                    getView().dataRequestCancelled();
                }
            }
        });

    }

    @Override
    public void cancelAsyncRequests() {
    }


    @Override
    public FlickrFeed getDataForSaveInstanceState() {
        return mCurrentData;
    }

    @Override
    public void setSavedInstanceState(FlickrFeed flickrFeed) {
        mDataFromSavedState = flickrFeed;
    }

    /**
     * simply redirect the action on the view
     * @param link
     */
    @Override
    public void viewRequestsOpenLink(String link) {
        if (isViewAttached()) {
            getView().openLinkInBrowser(link);
        }
    }
}
