package com.guidovezzoni.testtigerspike.viewholder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.guidovezzoni.testtigerspike.R;


/**
 * Created by guido on 09/12/16.
 */

public class FlickrFeedViewHolder extends RecyclerView.ViewHolder {
    public ImageView mImage;
    public TextView mTitle;
    public TextView mDatePublished;
    public TextView mAuthor;

    public FlickrFeedViewHolder(View itemView) {
        super(itemView);

        mImage = (ImageView) itemView.findViewById(R.id.image);
        mTitle = (TextView) itemView.findViewById(R.id.title);
        mDatePublished = (TextView) itemView.findViewById(R.id.date_published);
        mAuthor = (TextView) itemView.findViewById(R.id.author);
    }


}
