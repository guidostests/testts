package com.guidovezzoni.testtigerspike.utils;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.widget.Toast;

/**
 * Created by guido on 17/12/16.
 */

public class ShareHelper {

    /**
     * Simple routine to parse a link and creating an intent
     * If a browser is not installed than a toast will show
     *
     * @param context
     * @param url
     */
    public static void openBrowser(Context context, String url){
        if (!url.startsWith("https://") && !url.startsWith("http://")){
            url = "http://" + url;
        }
        Intent openUrlIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));

        if(openUrlIntent.resolveActivity(context.getPackageManager())==null) {
            Toast.makeText(context, "There is no browser installed", Toast.LENGTH_SHORT).show();
            return;
        }

        context.startActivity(openUrlIntent);
    }
}
