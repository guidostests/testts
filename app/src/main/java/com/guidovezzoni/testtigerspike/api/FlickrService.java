package com.guidovezzoni.testtigerspike.api;

import com.guidovezzoni.testtigerspike.model.FlickrFeed;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by guido on 09/12/16.
 */

public interface FlickrService {
    // sample: https://api.flickr.com/services/feeds/photos_public.gne?format=json&nojsoncallback=1
    @GET(Api.PHOTO_PUBLIC)
    Call<FlickrFeed> getPhotoPublic(@Query(Api.QUERY_FORMAT) String format,
                                    @Query(Api.QUERY_NOJSONCALLBACK) String nojsoncallback);
}
