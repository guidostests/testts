package com.guidovezzoni.testtigerspike.api;

/**
 * Created by guido on 09/12/16.
 */

public class Api {
    // query params
    public static final String QUERY_FORMAT = "format";
    public static final String QUERY_FORMAT_JSON = "json";

    public static final String QUERY_NOJSONCALLBACK = "nojsoncallback";
    public static final String QUERY_NOJSONCALLBACK_1 = "1";


    // root folders
    public static final String SERVICES = "services";
    public static final String FEEDS = "feeds";
    public static final String PHOTOS_PUBLIC = "photos_public.gne";


    // full endpoints
    public static final String PHOTO_PUBLIC = SERVICES + "/" + FEEDS + "/" + PHOTOS_PUBLIC;
}
