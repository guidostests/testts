package com.guidovezzoni.testtigerspike.model;

import org.parceler.Parcel;

import java.util.Date;

/**
 * Created by guido on 15/12/16.
 */
@Parcel
public class ItemFeed {
    public String title;
    public String link;
    public Media media;
    public String dateTaken;
    public String description;
    public Date published;
    public String author;
    public String authorId;
    public String tags;
}
