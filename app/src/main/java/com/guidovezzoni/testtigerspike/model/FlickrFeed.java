package com.guidovezzoni.testtigerspike.model;

import org.parceler.Parcel;

import java.util.List;

/**
 * Created by guido on 15/12/16.
 */

@Parcel
public class FlickrFeed {

    public String title;
    public String link;
    public String description;
    public String modified;
    public String generator;
    public List<ItemFeed> items = null;

}